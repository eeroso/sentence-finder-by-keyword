
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.*;

public class MyPanel extends JPanel implements ActionListener {
    private JButton hakuBtn;
    private JLabel jLabel1;
    private JTextField hakuSana;
    private JMenuBar menubar;
    private JCheckBox txtFile;
    private JTextArea tekstiAlue;
    final JFileChooser fc = new JFileChooser();
    public File file;
    public String hakusana;
    public static JFrame frame;
    
    public MyPanel() {
        //construct preComponents
        JMenu fileMenu = new JMenu ("File");
        JMenuItem avaa_tiedostoItem = new JMenuItem ("Avaa tiedosto");
        fileMenu.add (avaa_tiedostoItem);
        JMenuItem poistuItem = new JMenuItem ("Poistu");
        fileMenu.add (poistuItem);

        //construct components
        hakuBtn = new JButton ("Hae");
        jLabel1 = new JLabel ("Hakusana:");
        hakuSana = new JTextField (1);
        menubar = new JMenuBar();
        menubar.add (fileMenu);
        txtFile = new JCheckBox ("Tekstitiedosto");
        tekstiAlue = new JTextArea (20, 1);

        //adjust size and set layout
        setPreferredSize (new Dimension (1055, 650));
        setLayout (null);
        tekstiAlue.setLineWrap(true);
        tekstiAlue.setWrapStyleWord(true);

        //add components
        add (hakuBtn);
        add (jLabel1);
        add (hakuSana);
        add (menubar);
        add (txtFile);
        add (tekstiAlue);

        //set component bounds (only needed by Absolute Positioning)
        hakuBtn.addActionListener(this);  
        hakuBtn.setBounds (485, 205, 100, 30);
        jLabel1.setBounds (375, 130, 100, 25);
        hakuSana.setBounds (445, 130, 175, 25);
        menubar.setBounds (0, 0, 40, 20);
        txtFile.setBounds (480, 165, 120, 25);
        tekstiAlue.setBounds (20, 280, 1015, 360);

        avaa_tiedostoItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                int returnVal = fc.showOpenDialog(MyPanel.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();

                set_File(file);
            
            }
        }
    });



    hakuBtn.addActionListener(   
			new ActionListener () {
				public void actionPerformed(ActionEvent actEvent) {
                tekstiAlue.setText("");
                set_Text(hakuSana.getText());
                try {
                    search_sentences();	
                } catch(FileNotFoundException fe) {fe.printStackTrace();} 
                catch (IOException ie) {
                    ie.printStackTrace();
                }
        }
      }
    );
        
    }

    public static void set_frame(JFrame frame1){
        frame = frame1;
    }

    public static JFrame get_frame(){
        return frame;
    }

    public void search_sentences() throws FileNotFoundException, IOException {
        //FileReader fr1 = new FileReader(get_File());
        BufferedReader br1 = new BufferedReader(new InputStreamReader(new FileInputStream(get_File()),  "UTF-8"));
        ArrayList<String> words = new ArrayList();
        ArrayList<String> months = new ArrayList();
        ArrayList<String> lauseet = new ArrayList();
        ArrayList<String> test = new ArrayList();
        PrintWriter writer = new PrintWriter("rivit.txt", "UTF-8");
        String str="";
        String linet="";
        months.add(" tammikuu");
        months.add(" helmikuu");
        months.add(" maaliskuu");
        months.add(" huhtikuu");
        months.add(" toukokuu");
        months.add(" kesäkuu");
        months.add(" heinäkuu");
        months.add(" elokuu");
        months.add(" syyskuu");
        months.add(" lokakuu");
        months.add(" marraskuu");
        months.add(" joulukuu");
        
        //päivämäärät joissa piste päivän perässä. Normimuodossa ja -ta päätteellä (esimerkiksi 21. marraskuu & 21. marraskuuta)
        for(int m = 0; m<months.size(); m++){
            for(int d = 1; d<32; d++){
                test.add(String.valueOf(d)+"."+months.get(m));
                test.add(String.valueOf(d)+"."+months.get(m)+"ta");
            }
        }
        String [] special_sanat = new String[test.size()+5];
        for (int x = 0; x<test.size(); x++){
            special_sanat[x] = test.get(x);
        }

        //lyhenteet joissa on .
        special_sanat[test.size()] = "esim.";
        special_sanat[test.size()+1] = "yms.";
        special_sanat[test.size()+2] = "jne.";
        special_sanat[test.size()+3] = "tms.";
        special_sanat[test.size()+4] = "etc.";

        String sanat = get_Text();
        for(String w: sanat.split(", ")){
            words.add(w);
        }
        while ((str = br1.readLine()) != null){
            linet += str;
        }
       Pattern p = buildRegexToFindSentencesContaining(words.get(0), special_sanat);

       for (Matcher m = p.matcher(linet); m.find(); )
       lauseet.add(m.group().replaceAll("\\s+", " ").trim());

       if(!txtFile.isSelected()){
           for(String l : lauseet){
               tekstiAlue.append(l);
           }
       } else {
            for(String l : lauseet){
                writer.println(l);
            }
            JOptionPane.showMessageDialog(get_frame(),
                        "Lauseet tallennettu tekstitiedostoon 'rivit.txt'");
           
       }
       writer.close();
    }

    public Pattern buildRegexToFindSentencesContaining(String word, String[] specials) {
        StringJoiner regexText = new StringJoiner("|", "(?:", "|[^.!?])*").setEmptyValue("[^.!?]*");
        for (String s : specials)
            regexText.add(toWordRegex(s));
        String regex = regexText + toWordRegex(word) + regexText + "[.!?]";
        return Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
    }

    private String toWordRegex(String word) {
        String regex = Pattern.quote(word);
        if (word.matches("\\b.*"))
            regex = "\\b" + regex;
        if (word.matches(".*\\b"))
            regex = regex + "\\b";
        return regex;
    }

    public void set_File(File file){
        this.file = file;
    }

    public File get_File(){
        return this.file;
    }
   
    public void set_Text(String text){
        this.hakusana = text;
    }

    public String get_Text(){
        return this.hakusana;
    }
    


    public static void main (String[] args) {
        JFrame frame = new JFrame ("MyPanel");
        frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add (new MyPanel());
        frame.pack();
        frame.setVisible (true);
        set_frame(frame);

        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub

    }
}
